from setuptools import find_packages, setup

VERSION = '22.50.0.1'  # This is updated by git flow

setup(
    name='unicroncat',
    version=VERSION,
    description='Cats are just so super cute',
    url='https://httpcat.us',
    author='Brie Carranza',
    author_email='bcarranza@gitlab.com',
    packages=find_packages(exclude=['tests*']),
    include_package_data=True,
    install_requires=[
        'PyYAML',
        'opentelemetry-instrumentation==0.36b0',
        'opentelemetry-instrumentation-flask',
        'opentelemetry-instrumentation-psycopg2',
        'opentelemetry-instrumentation-requests',
        'opentelemetry-sdk',
        'opentelemetry-exporter-otlp-proto-http',
    ]
)